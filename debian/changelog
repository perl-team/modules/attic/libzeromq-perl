libzeromq-perl (0.23-2) UNRELEASED; urgency=low

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

 -- Salvatore Bonaccorso <carnil@debian.org>  Sun, 06 Jan 2013 22:09:31 +0100

libzeromq-perl (0.23-1) unstable; urgency=low

  * Team upload.
  * New upstream release.
  * Add deprecation notice to debian/NEWS.
  * Update years of copyright for inc/Module/*.
  * Bump Standards-Version to 3.9.4 (no changes).

 -- gregor herrmann <gregoa@debian.org>  Sat, 15 Dec 2012 21:40:51 +0100

libzeromq-perl (0.21-2) unstable; urgency=low

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Alessandro Ghedini ]
  * Email change: Alessandro Ghedini -> ghedo@debian.org
  * Add fix-checklib-fail.patch

 -- Alessandro Ghedini <ghedo@debian.org>  Wed, 08 Aug 2012 16:53:58 +0200

libzeromq-perl (0.21-1) unstable; urgency=low

  * New upstream release
  * Update debian/copyright format as in Debian Policy 3.9.3
  * Bump Standards-Version to 3.9.3
  * Bump debhelper compat level to 9
  * Add dont-link-to-uuid.patch

 -- Alessandro Ghedini <al3xbio@gmail.com>  Thu, 22 Mar 2012 20:09:20 +0100

libzeromq-perl (0.20-1) unstable; urgency=low

  * New upstream release

 -- Alessandro Ghedini <al3xbio@gmail.com>  Fri, 13 Jan 2012 20:49:09 +0100

libzeromq-perl (0.19-1) unstable; urgency=low

  [ gregor herrmann ]
  * Swap order of alternative (build) dependencies after the perl 5.14 
    transition.

  [ Alessandro Ghedini ]
  * New upstream release
  * Update Build-Depends
  * Sort Build-Depends
  * Remove libdevel-checklib-perl from B-D (embedded in dist's source)

 -- Alessandro Ghedini <al3xbio@gmail.com>  Thu, 08 Dec 2011 16:53:25 +0100

libzeromq-perl (0.18-1) unstable; urgency=low

  * New upstream release

 -- Alessandro Ghedini <al3xbio@gmail.com>  Mon, 07 Nov 2011 20:40:16 +0100

libzeromq-perl (0.17-1) unstable; urgency=low

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.

  [ Alessandro Ghedini ]
  * New upstream release

 -- Alessandro Ghedini <al3xbio@gmail.com>  Fri, 09 Sep 2011 16:49:49 +0200

libzeromq-perl (0.16-1) unstable; urgency=low

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Alessandro Ghedini ]
  * New upstream release

 -- Alessandro Ghedini <al3xbio@gmail.com>  Tue, 02 Aug 2011 16:41:04 +0200

libzeromq-perl (0.15-1) unstable; urgency=low

  * Team upload.

  [ Alessandro Ghedini ]
  * New upstream release

  [ gregor herrmann ]
  * Fix typo in Depends, it's -weaken-; thanks to Mehdi Dogguy for the bug
    report (closes: #628739).

 -- gregor herrmann <gregoa@debian.org>  Wed, 01 Jun 2011 00:14:04 +0200

libzeromq-perl (0.14-1) unstable; urgency=low

  * Initial Release (Closes: #619035)

 -- Alessandro Ghedini <al3xbio@gmail.com>  Sun, 22 May 2011 20:43:05 +0200
